import {describe, expect, test} from '@jest/globals';
const matching_pair = require('../point_4/answer')

describe('Matching pair module', () => {
  test('[2,3,6,7] - sum = 9 - Successful', () => {
    expect(matching_pair([2,3,6,7], 9)).toBe('OK, matching pair (3, 6)');
  });

  test('[1,3,3,7]  sum = 9 - Wrong', () => {
    expect(matching_pair([1,3,3,7], 9)).toBe('No');
  });
});