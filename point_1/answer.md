# The last you see I use RTMF and LMGTFY

I like to **constantly improve** the things that I am applying for and one of them is **software engineering**, so I go from the years I have improved my ability to perform Google searches and look for the official documentation of the tools I am using.

- RTFM: Currently, I am studying to improve my ability to solve algorithms, for this, I have been reading books on algorithmic complexity and strategies to solve them.

- LMGTFY: When I was doing this test, basically because I used it to search and read almost all the things I didn't know.

# The OS that You use

I currently use the **Linux** operating system in its **Ubuntu** distribution.
I have also worked from the Windows operating system, but using WSL or a virtual machine with Ubuntu for development.

# Languages You Master

I am a master in the Javascript programming language and its ecosystem:
- React.js
- TypeScript
- JSX
- Material-UI
- Node.js
- Express.js
- Jest

Also, I master in: 
- Git
- Commands Line 
- HTML
- CSS