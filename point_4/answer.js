/* 
Assumptions
1. the collection always will be an array
2. the collection has only one type of 'number'
3. if the collection has less than one element the response is 'NO'
4. the sum value always is a type 'number'
5. if the sum value has another type return 'NO'
6. the values have to be continuous
7. the answer when the values match is 
`OK, matching pair (num_1, num_2)` 
8. the first function´s argument is the collection and the second function's 
argument is the sum
9. if any values of the collection are a type different from the number, this won't take care
*/

const matching_pair = ( arr, sum_value ) => {
    if (typeof sum_value !== 'number') return 'No'
    
    let p1, p2
    for (p1 = 0; p1 < arr.length - 1; p1++) {
        p2 = p1 + 1
        const num_1 = arr[p1]
        const num_2 = arr[p2]
        if ( (num_1 + num_2) === sum_value ) {
            return `OK, matching pair (${num_1}, ${num_2})`
        }
    }

    return 'No'
}

module.exports = matching_pair