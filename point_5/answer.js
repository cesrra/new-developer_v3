function hexaToAscii(hex_message) {
    let ascii = '';
    for (let i = 0; i < hex_message.length; i += 2) {
      const hexChar = hex_message.substr(i, 2);
      const decimalChar = parseInt(hexChar, 16);
      ascii += String.fromCharCode(decimalChar);
    }

    return ascii;
}

const hide_message = () => {
    const first_part = "4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c206167726567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f7220617175ed212e"
    const secound_part = "U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVuIG1lbnNhamUu"
    
    const message = `${hexaToAscii(first_part)}\n${Buffer.from(secound_part, 'base64').toString('utf-8')}`
    return message
}

module.exports = hide_message